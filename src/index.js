import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import store from "./store";
import { ThemeProvider, createTheme } from "@material-ui/core/styles";

const theme = createTheme({
  palette: {
    primary: {
      main: "#1480fb",
    },

    secondary: {
      main: "#08043c",
    },

    success: {
      main: "#00ffff",
    },

    info: {
      main: "#6d0c91",
    },
  },
  typography: {
    h2: {
      fontFamily: "Open Sans, sans-serif",
      fontSize: "2rem",
      fontWeight: "bold",
    },

    h3: {
      fontFamily: "Otomanopee One, sans-serif",
      fontSize: "2rem",
    },

    h4: {
      fontFamily: "Otomanopee One, sans-serif",
      fontSize: "1.5rem",
    },

    h5: {
      fontFamily: "Open Sans, sans-serif",
    },

    h6: {
      fontFamily: "Open Sans, sans-serif",
    },
  },
});

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <BrowserRouter>
          <App />
        </BrowserRouter>
      </ThemeProvider>
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);
