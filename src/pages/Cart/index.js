import {
  Grid,
  Typography,
  Button,
  Card,
  Box,
  Container,
} from "@material-ui/core";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import ProductInCart from "../../components/ProductInCart";
import { makeStyles } from "@material-ui/core/styles";

const useStyle = makeStyles({
  order: {
    padding: "25px",
  },
});

const Cart = () => {
  const classes = useStyle();

  const cart = useSelector((store) => store.cart);

  const history = useHistory();

  return (
    <>
      <Container>
        <Box p={3}>
          <Typography
            variant="h3"
            component="h1"
            align="center"
            color="secondary"
          >
            Carrinho de compras
          </Typography>
        </Box>
        <Grid container spacing={2}>
          <Grid item xs={12} sm={8}>
            <Grid container spacing={1}>
              {cart.map((product, index) => (
                <ProductInCart key={index} product={product} index={index} />
              ))}
            </Grid>
          </Grid>
          <Grid item xs={12} sm={4} p={2}>
            <Card className={classes.order}>
              <Typography variant="h4">Resumo do pedido</Typography>
              <Typography variant="subtitle1">
                Quantidade:{" "}
                {cart.length === 0
                  ? "Carrinho vazio"
                  : cart.length === 1
                  ? `1 produto`
                  : `${cart.length} produtos`}
              </Typography>
              <Typography variant="h2">
                Total: R$
                {cart.reduce((total, product) => total + product.price, 0)}
              </Typography>
            </Card>
          </Grid>
        </Grid>
      </Container>
      <Box m={5} textAlign="center">
        <Button
          variant="contained"
          color="secondary"
          onClick={() => history.push("/")}
        >
          Voltar
        </Button>
      </Box>
    </>
  );
};

export default Cart;
